<?php

/**
 * The plugin bootstrap file
 *
 * This file is read by WordPress to generate the plugin information in the plugin
 * admin area. This file also includes all of the dependencies used by the plugin,
 * registers the activation and deactivation functions, and defines a function
 * that starts the plugin.
 *
 * @link              http://www.sajdnota.pl
 * @since             1.0.0
 * @package           Bliss_Acf
 *
 * @wordpress-plugin
 * Plugin Name:       Bliss ACF Generator
 * Plugin URI:        http://dev.sajdnota.pl
 * Description:       This is a short description of what the plugin does. It's displayed in the WordPress admin area.
 * Version:           1.0.0
 * Author:            Przemek Cichon
 * Author URI:        http://www.sajdnota.pl
 * License:           GPL-2.0+
 * License URI:       http://www.gnu.org/licenses/gpl-2.0.txt
 * Text Domain:       bliss-acf
 * Domain Path:       /languages
 */

// If this file is called directly, abort.
if ( ! defined( 'WPINC' ) ) {
	die;
}

/**
 * The code that runs during plugin activation.
 * This action is documented in includes/class-bliss-acf-activator.php
 */
function activate_bliss_acf() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bliss-acf-activator.php';
	Bliss_Acf_Activator::activate();
}

/**
 * The code that runs during plugin deactivation.
 * This action is documented in includes/class-bliss-acf-deactivator.php
 */
function deactivate_bliss_acf() {
	require_once plugin_dir_path( __FILE__ ) . 'includes/class-bliss-acf-deactivator.php';
	Bliss_Acf_Deactivator::deactivate();
}

register_activation_hook( __FILE__, 'activate_bliss_acf' );
register_deactivation_hook( __FILE__, 'deactivate_bliss_acf' );

/**
 * The core plugin class that is used to define internationalization,
 * admin-specific hooks, and public-facing site hooks.
 */
require plugin_dir_path( __FILE__ ) . 'includes/class-bliss-acf.php';

/**
 * Begins execution of the plugin.
 *
 * Since everything within the plugin is registered via hooks,
 * then kicking off the plugin from this point in the file does
 * not affect the page life cycle.
 *
 * @since    1.0.0
 */
function run_bliss_acf() {

	$plugin = new Bliss_Acf();
	$plugin->run();

}
run_bliss_acf();
