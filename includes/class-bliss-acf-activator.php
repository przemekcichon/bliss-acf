<?php

/**
 * Fired during plugin activation
 *
 * @link       http://www.sajdnota.pl
 * @since      1.0.0
 *
 * @package    Bliss_Acf
 * @subpackage Bliss_Acf/includes
 */

/**
 * Fired during plugin activation.
 *
 * This class defines all code necessary to run during the plugin's activation.
 *
 * @since      1.0.0
 * @package    Bliss_Acf
 * @subpackage Bliss_Acf/includes
 * @author     Przemek Cichon <przemekcichon@gmail.com>
 */
class Bliss_Acf_Activator {

	/**
	 * Short Description. (use period)
	 *
	 * Long Description.
	 *
	 * @since    1.0.0
	 */
	public static function activate() {

	}

}
