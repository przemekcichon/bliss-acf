<?php

class Bliss_Acf_Add_Field {

    public function __construct($array) {
        $this->add_field($array);
    }
    
    protected function add_field($array) {
        acf_add_local_field($array);
    }
    
}