<?php

class Bliss_Acf_Add_Group {
    
    protected $defaults = array(
        'fields' => array(),
        'location' => array (
            array (
                array (
                    'param' => 'page_type',
                    'operator' => '==',
                    'value' => 'front_page',
                ),
            ),
        ),
        'menu_order' => 0,
        'position' => 'normal',
        'style' => 'default',
        'label_placement' => 'top',
        'instruction_placement' => 'label',
        'hide_on_screen' => array (
            0 => 'permalink',
            1 => 'the_content',
            2 => 'excerpt',
            3 => 'custom_fields',
            4 => 'discussion',
            5 => 'comments',
            6 => 'revisions',
            7 => 'slug',
            8 => 'author',
            9 => 'format',
            10 => 'page_attributes',
            11 => 'featured_image',
            12 => 'categories',
            13 => 'tags',
            14 => 'send-trackbacks',
        ),
    );


    public function __construct($array) {
        $args = wp_parse_args($array, $this->defaults);
        $this->add_group($args);
    }
    
    protected function add_group($array) {
        if( function_exists('acf_add_local_field_group') ):
            acf_add_local_field_group($array);
        endif;
    }
}
